#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from SCP_sprout device
$(call inherit-product, device/hmd/SCP_sprout/device.mk)

PRODUCT_DEVICE := SCP_sprout
PRODUCT_NAME := lineage_SCP_sprout
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia G21
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="ShadowcatPlus_Natv-user 13 TP1A.220624.014 V3.430_B01 release-keys"

BUILD_FINGERPRINT := Nokia/ShadowcatPlus_00WW/SCP_sprout:13/TP1A.220624.014/00WW_3_430:user/release-keys
