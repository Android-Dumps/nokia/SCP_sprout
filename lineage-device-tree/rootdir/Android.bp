//
// Copyright (C) 2023 The LineageOS Project
//
// SPDX-License-Identifier: Apache-2.0
//

// Init scripts
sh_binary {
    name: "loading.sh",
    src: "bin/loading.sh",
    vendor: true,
}

sh_binary {
    name: "total.sh",
    src: "bin/total.sh",
    vendor: true,
}

sh_binary {
    name: "log_to_csv.sh",
    src: "bin/log_to_csv.sh",
    vendor: true,
}

sh_binary {
    name: "para.sh",
    src: "bin/para.sh",
    vendor: true,
}

sh_binary {
    name: "init.insmod.sh",
    src: "bin/init.insmod.sh",
    vendor: true,
}

sh_binary {
    name: "zramwb.sh",
    src: "bin/zramwb.sh",
    vendor: true,
}

sh_binary {
    name: "idlefast.sh",
    src: "bin/idlefast.sh",
    vendor: true,
}

// Init configuration files
prebuilt_etc {
    name: "init.cali.rc",
    src: "etc/init.cali.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ShadowcatPlus.rc",
    src: "etc/init.ShadowcatPlus.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_zebu.usb.rc",
    src: "etc/init.ums9230_zebu.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.Shadowcat.usb.rc",
    src: "etc/init.Shadowcat.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.zramwb.rc",
    src: "etc/init.zramwb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_4h10_go.rc",
    src: "etc/init.ums9230_4h10_go.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ram.rc",
    src: "etc/init.ram.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_1h10.rc",
    src: "etc/init.ums9230_1h10.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ram.gms.rc",
    src: "etc/init.ram.gms.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_zebu.rc",
    src: "etc/init.ums9230_zebu.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ram.native.rc",
    src: "etc/init.ram.native.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_1h10_go.rc",
    src: "etc/init.ums9230_1h10_go.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.hmd_custom.rc",
    src: "etc/init.hmd_custom.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.Magik.usb.rc",
    src: "etc/init.Magik.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.Shadowcat.rc",
    src: "etc/init.Shadowcat.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.Magik.rc",
    src: "etc/init.Magik.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_4h10.usb.rc",
    src: "etc/init.ums9230_4h10.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ShadowcatPlus.usb.rc",
    src: "etc/init.ShadowcatPlus.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_6h10.rc",
    src: "etc/init.ums9230_6h10.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_6h10.usb.rc",
    src: "etc/init.ums9230_6h10.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_1h10_go.usb.rc",
    src: "etc/init.ums9230_1h10_go.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_4h10.rc",
    src: "etc/init.ums9230_4h10.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_4h10_go.usb.rc",
    src: "etc/init.ums9230_4h10_go.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.storage.rc",
    src: "etc/init.storage.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_1h10.usb.rc",
    src: "etc/init.ums9230_1h10.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_haps.rc",
    src: "etc/init.ums9230_haps.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.ums9230_haps.usb.rc",
    src: "etc/init.ums9230_haps.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

// fstab
prebuilt_etc {
    name: "fstab.ums9230_zebu",
    src: "etc/fstab.ums9230_zebu",
    vendor: true,
}
