#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_SCP_sprout.mk

COMMON_LUNCH_CHOICES := \
    lineage_SCP_sprout-user \
    lineage_SCP_sprout-userdebug \
    lineage_SCP_sprout-eng
